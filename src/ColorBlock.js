import React from "react";
import PropTypes from "prop-types";
import { DropTarget } from "react-dnd";

/**
 * Implements the drag source contract.
 */
const cardSource = {
    beginDrag(props) {
        return {
            text: props.text
        };
    }
};

/**
 * Specifies the props to inject into your component.
 */
function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    };
}

const propTypes = {
    // Injected by React DnD:
    isDragging: PropTypes.bool.isRequired,
    connectDragSource: PropTypes.func.isRequired
};

class ColorBlock extends React.Component {
    render() {
        const { isDragging, connectDragSource, text } = this.props;
        let style = {
            background:
                "rgb(" +
                Math.floor(255 * this.props.c.r) +
                "," +
                Math.floor(255 * this.props.c.g) +
                "," +
                Math.floor(255 * this.props.c.b) +
                ")"
        };
        return connectDragSource(<div className="color-block" style={style} />);
    }
}

ColorBlock.propTypes = propTypes;

export default DragSource()(ColorBlock);