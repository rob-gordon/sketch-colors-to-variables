import React from "react";
import { render } from "react-dom";
import tinycolor from "tinycolor2";

class ColorBlock extends React.Component {
    dragStart(e) {
        this.dragged = e.currentTarget;
        e.dataTransfer.effectAllowed = "move";

        // Firefox requires calling dataTransfer.setData
        // for the drag to properly work
        e.dataTransfer.setData("text/html", e.currentTarget);
    }

    dragEnd(e) {
        this.props.colorBlockDragEnd(this.props.c);
    }

    getHexColor() {
        return tinycolor.fromRatio(this.props.c).toHexString();
    }

    getHexClasses() {
        return (
            "hex" + (tinycolor.fromRatio(this.props.c).isDark() ? " dark" : "")
        );
    }

    getTitleClasses() {
        return (
            "title" +
            (tinycolor.fromRatio(this.props.c).isDark() ? " dark" : "")
        );
    }

    changeTitle(e) {
        let _title = e.target.value;
        this.props.changeColorTitle(this.props.c, _title);
    }

    render() {
        let style = {
            background:
                "rgb(" +
                Math.floor(255 * this.props.c.r) +
                "," +
                Math.floor(255 * this.props.c.g) +
                "," +
                Math.floor(255 * this.props.c.b) +
                ")"
        };
        return (
            <div
                className="color-block"
                style={style}
                draggable="true"
                onDragEnd={this.dragEnd.bind(this)}
                onDragStart={this.dragStart.bind(this)}
            >
                <span className={this.getHexClasses()}>
                    {this.getHexColor()}
                </span>
                <input
                    className={this.getTitleClasses()}
                    value={this.props.c.title}
                    onChange={this.changeTitle.bind(this)}
                />
            </div>
        );
    }
}

class GroupBlock extends React.Component {
    onDragEnter(e) {
        this.props.setDragFocusGroup(this.props.g.id);
        e.preventDefault();
    }

    onDragExit(e) {
        this.props.setDragFocusGroup(false);
        e.preventDefault();
    }

    changeGroupName(e) {
        let newName = e.target.value;
        this.props.changeGroupName(this.props.g.id, newName);
    }

    colorBlockDragEnd(color) {
        this.props.colorBlockDragEnd(color, this.props.g.id);
    }

    changeColorTitle(color, title) {
        console.log(color, title, this.props.g.id);
        this.props.changeColorTitle(color, title, this.props.g.id);
    }

    render() {
        let colors = this.props.g.colors.map(color => {
            return (
                <ColorBlock
                    c={color}
                    colorBlockDragEnd={this.colorBlockDragEnd.bind(this)}
                    changeColorTitle={this.changeColorTitle.bind(this)}
                />
            );
        });

        return (
            <div
                className="group-block"
                onDragEnter={this.onDragEnter.bind(this)}
                onDragExit={this.onDragExit.bind(this)}
            >
                <input
                    type="text"
                    value={this.props.g.title}
                    onChange={this.changeGroupName.bind(this)}
                />
                {colors}
            </div>
        );
    }
}

class App extends React.Component {
    componentWillMount() {
        this.setState({
            colors: [],
            groups: [],
            groupId: 1,
            focusGroupId: false
        });
    }
    componentDidMount() {
        fetch("data.json")
            .then(res => {
                return res.json();
            })
            .then(res => {
                let i = 0;
                res.forEach(c => {
                    c.id = ++i;
                    c.title = c.id;
                });
                this.setState({ colors: res });
            });
    }

    setDragFocusGroup(id) {
        console.log("Group: " + id);
        this.setState({ focusGroupId: id });
    }

    colorBlockDragEnd(color, curGroupId) {
        if (this.state.focusGroupId !== false) {
            let colors = this.state.colors;
            let groups = this.state.groups;
            let groupId = this.state.focusGroupId;

            // remove from group
            if (curGroupId) {
                let curGroup = groups.filter(_g => _g.id == curGroupId)[0];
                let index = curGroup.colors.findIndex(_c => {
                    return _c.id == color.id;
                });
                let _color = curGroup.colors.splice(index, 1)[0];
            } else {
                let index = colors.findIndex(_c => {
                    return _c.id == color.id;
                });
                let _color = colors.splice(index, 1)[0];
            }

            let focusGroup = groups.filter(g => g.id == groupId)[0];
            focusGroup.colors.push(color);

            this.setState({ colors, groups, focusGroupId: false });
        }
    }

    addGroup() {
        let groups = this.state.groups;
        let groupId = this.state.groupId;
        groups.push({ title: "Untitled", id: groupId, colors: [] });
        groupId = groupId + 1;
        this.setState({ groups, groupId });
    }

    changeGroupName(id, newName) {
        let groups = this.state.groups;
        let ourGroup = groups.filter(g => g.id == id)[0];
        ourGroup.title = newName;
        this.setState({ groups });
    }

    changeColorTitle(color, title, groupId = false) {
        let colors = this.state.colors;
        let groups = this.state.groups;
        let _color;
        if (groupId) {
            let _group = groups.filter(g => g.id == groupId)[0];
            _color = _group.colors.filter(c => c.id == color.id)[0];
        } else {
            _color = colors.filter(c => c.id == color.id)[0];
        }
        _color.title = title;
        this.setState({ groups, colors });
    }

    sort_hue(a, b) {
        let _a = tinycolor.fromRatio(a);
        let _b = tinycolor.fromRatio(b);
        return _a.toHsl().h - _b.toHsl().h;
    }

    sort_lightness(a, b) {
        let _a = tinycolor.fromRatio(a);
        let _b = tinycolor.fromRatio(b);
        return _a.toHsl().l - _b.toHsl().l;
    }

    sort_saturation(a, b) {
        let _a = tinycolor.fromRatio(a);
        let _b = tinycolor.fromRatio(b);
        return _a.toHsl().s - _b.toHsl().s;
    }

    sort_value(a, b) {
        let _a = tinycolor.fromRatio(a);
        let _b = tinycolor.fromRatio(b);
        return _a.toHsv().v - _b.toHsv().v;
    }

    sort(kind) {
        let colors = this.state.colors;
        colors.sort(this["sort_" + kind]);
        let groups = this.state.groups;
        groups.forEach(g => {
            let colors = g.colors;
            colors.sort(this["sort_" + kind]);
        });
        this.setState({ colors, groups });
    }

    sassOutput() {
        let output = "";
        this.state.groups.forEach((g, i) => {
            let prefix = g.title;
            if (g.colors.length) {
                g.colors.forEach(c => {
                    let expand = [prefix];
                    if (c.title !== "") {
                        expand.push(c.title);
                    }
                    let variableName = expand.join("-");
                    let hex = tinycolor
                    .fromRatio(c)
                    .toHexString();
                    let dark = tinycolor.fromRatio(c).isLight() ? " class=\"light\"" : "";
                    output += `<span style="color: ${hex};"${dark}>$${variableName}: ${hex};</span>`;
                });
                console.log(i, this.state.groups.length);
                if (i !== this.state.groups.length - 1) {
                    output += "<br>";
                }
            }
        });
        return {__html: output};
    }

    render() {
        let colors = this.state.colors.map(color => {
            return (
                <ColorBlock
                    c={color}
                    colorBlockDragEnd={this.colorBlockDragEnd.bind(this)}
                    changeColorTitle={this.changeColorTitle.bind(this)}
                />
            );
        });

        let groups = this.state.groups.map(group => {
            return (
                <GroupBlock
                    g={group}
                    changeGroupName={this.changeGroupName.bind(this)}
                    setDragFocusGroup={this.setDragFocusGroup.bind(this)}
                    colorBlockDragEnd={this.colorBlockDragEnd.bind(this)}
                    changeColorTitle={this.changeColorTitle.bind(this)}
                />
            );
        });

        return (
            <div>
                <div className="container">
                    <div className="controls">
                        <button onClick={this.sort.bind(this, "hue")}>
                            Sort Hue
                        </button>
                        <button onClick={this.sort.bind(this, "saturation")}>
                            Sort Saturation
                        </button>
                        <button onClick={this.sort.bind(this, "lightness")}>
                            Sort Lightness
                        </button>
                        <button onClick={this.sort.bind(this, "value")}>
                            Sort Value
                        </button>
                    </div>
                    <div className="color-list">{colors}</div>
                    <div className="controls controls-right">
                        <button onClick={this.addGroup.bind(this)}>
                            Add Prefix Group
                        </button>
                    </div>
                    {groups}
                </div>
                <div className="output">
                    <div className="inner" dangerouslySetInnerHTML={this.sassOutput()}></div>
                </div>
            </div>
        );
    }
}

render(<App />, document.getElementById("app"));
