const fs = require("fs");
const path = require("path");
const unzip = require("unzip");
const rimraf = require("rimraf");
const colorDiff = require("color-difference");
const tinycolor = require("tinycolor2");
const sketchFile = process.argv[2];
const unpackDir = path.resolve(__dirname, "output");
const browserSync = require("browser-sync");

if (!sketchFile) {
    console.error("No sketch file supplied.");
    return;
}

const colors = [];

fs.createReadStream(sketchFile).pipe(
    unzip.Extract({ path: unpackDir }).on("finish", () => {
        const doc = JSON.parse(
            fs.readFileSync(path.resolve("output/document.json"), "utf-8")
        );

        if (doc.assets.colors.length == 0) {
            console.error("Document has no global colors.");
            return;
        } else {
            doc.assets.colors.forEach((c, i) => {
                colors.push({
                    r: c.red,
                    g: c.green,
                    b: c.blue,
                    a: c.alpha
                });
            });
    
            fs.writeFileSync(
                path.resolve(__dirname, "data.json"),
                JSON.stringify(colors)
            );

            browserSync({server: "./"});
        }
    })
);
